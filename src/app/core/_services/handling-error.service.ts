import {Injector} from "@angular/core";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {ShareUiService} from "../../services/share_ui.service";


export class HandlingErrorService {

  constructor(
    public router: Router,
    ) {
  }

}

export function handlingError(error: any, shareUi: ShareUiService) {
  const errorData = error.error;
  if (error.status === 422) {
    Object.keys(errorData.message).forEach((value) => {
      shareUi.ShowErrorAlert('Error!', errorData.message[value]);
    });
  }
  if (error.status === 401) {
    shareUi.ShowErrorAlert('Error!', 'Unauthorized please login');
  }
  if (error.status === 404) {
    shareUi.ShowErrorAlert('Error!', errorData.message);
  }
  else{
    shareUi.ShowErrorAlert('Error!', errorData.message);
  }
}
