import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BaseApiUrl} from "../_config/base-url";
import {User} from "../_api/user.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept-Language': 'en, ar',
  })
};

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient,
  ) { }

  get(options: GetOptions): Observable<any> {
    return this.http.get<any>(options.url, httpOptions);
  }


}

// ----------------------------------------------------------------------------------- //
// ----------------------------------------------------------------------------------- //
export interface Params {
  [key: string]: any;
}

export interface GetOptions {
  url: string;
  params?: Params;
}

export interface ErrorResponse {
  id: string;
  code: string;
  message: string;
}
