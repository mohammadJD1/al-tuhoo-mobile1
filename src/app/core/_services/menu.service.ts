import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private menuOpenSource = new BehaviorSubject(false);
  currentMenuOpen = this.menuOpenSource.asObservable();

  constructor(

  ) {
  }

  changeMenuOpen(val){
    this.menuOpenSource.next(val);
  }

}
