export let Public = '';
export let ArabicLang = false;

// export let BaseAppUrl = 'http://165.22.114.195:7080';
export let BaseAppUrl = 'https://api.al-tuhoo.com';
// export let BaseAppUrl = 'http://127.0.0.1:8000';
export let BaseApiUrl = BaseAppUrl + '/api';
export let BaseImageUrl = BaseApiUrl;
