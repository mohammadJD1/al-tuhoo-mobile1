import {BaseModel} from './base.model';
import {Translate} from "./translate.model";
import {BrandsData} from "./brands.model";
import {TranslateValue} from "./translateValue.model";


export class SingleProduct extends BaseModel{
  data: ProductData = this.init();

  init(){
    let item: ProductData = new ProductData();
    item = item.init();
    return item;
  }
}

export class ProductData{
  id: number;
  sku: string;
  main_photo:  string;
  rate:number;
  badge:  string;
  active: number;
  low_stock_threshold: number;
  virtual: number;
  brand: BrandsData = new BrandsData();
  count_reviews: number;
  images: Array<Images> = [];
  translations: {
    "en": {
      name: string,
      "description": string,
      "specifications": string
    },
    "ar": {
      "name": string,
      "description": string,
      "specifications": string
    }
  };
  items: Array<ProdItem>;
  is_favorite:boolean;

  init(){
    let item: ProductData = new ProductData();
    item.translations = {
      "en": {
        name: '',
        "description": '',
        "specifications": ''
      },
      "ar": {
        "name": '',
        "description": '',
        "specifications": ''
      }
    };
    // item.translations.en.name ='';
    // item.translations.ar.name ='';
    return item;
  }
}

export class Product extends BaseModel{
  data: Array<ProductData> = new Array<ProductData>();
  init(){
    for(let i =0 ;i<this.data.length ;i++){
      this.data[i].init();
    }
  }
}

export class ProdItem{
  id: number = -1;
  quantity: number = 0;
  price: number ;
  new_price: number ;
  color: string ;
  size: Size ;
  items_attributes: Array<Attributes>;
  code: string;
  ordered_quantity: number;
  pieces: string;
}

export class Attributes extends TranslateValue{
  id: number ;
  attribute: {
    id: number ;
    key: string ;
    translations: {
      "en": {
        "name": string
      },
      "ar": {
        "name": string
      }
    }
  };
}

export class ProductTranslate{
  en: {
    name: string,
    "description": string,
    "specifications": string
  };
  ar: {
    "name": string,
    "description": string,
    "specifications": string
  }
}

export class Size extends Translate{
  id: number;
  code: string;
}

export class Images extends Translate{
  created_at: string ;
  id: number ;
  image:  string;
  product_id: number ;
  updated_at:string ;
}

