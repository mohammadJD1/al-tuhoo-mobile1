import {BaseModel} from './base.model';
import {Translate} from "./translate.model";


export class SingleBrands extends BaseModel{
  data: BrandsData;
}

export class BrandsData extends Translate{
  id: number;
  code: string;
}

export class Brands extends BaseModel{
  data: Array<BrandsData> = [];
}
