import {BaseModel} from './base.model';
import {Translate} from "./translate.model";


export class SingleMyAccount extends BaseModel{
  data: MyAccountData;
}

export class MyAccountData{
  birthday: string ;
  created_at: string ;
  email: string ;
  first_name: string ;
  home_address1: string ;
  home_address2: string ;
  id: number;
  last_name: string ;
  location_address1: string ;
  location_address2: string ;
  mobile: string ;
  photo: string ;
  updated_at: string ;
  username: string ;
  work_address: string ;
}

export class MyAccount extends BaseModel{
  data: MyAccountData = new MyAccountData();
}
