import {BaseModel} from './base.model';
import {Translate} from "./translate.model";


export class SingleArticlesPage extends BaseModel{
  data: ArticlesPageData;
}

export class ArticlesPageData extends Translate{
  id: number;
  code: string;
}

export class ArticlesPage extends BaseModel{
  data: Array<ArticlesPageData> = [];
}
