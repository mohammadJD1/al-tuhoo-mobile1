export class Translate{
  translations: {
    en: {
      name: string
    },
    ar: {
      name: string
    }
  };
  constructor(){
    this.translations = {
      en: {
        name: ''
      },
      ar: {
        name: ''
      }
    };
  }
}
