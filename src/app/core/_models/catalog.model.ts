import {BaseModel} from './base.model';

export class Translate{
  translations: {
    en: {
      name: string,
      description: string,
    },
    ar: {
      name: string,
      description: string,
    }
  };
  constructor(){
    this.translations = {
      en: {
        name: '',
        description: '',
      },
      ar: {
        name: '',
        description: '',
      }
    };
  }
}

export class SingleCatalog extends BaseModel{
  data: CatalogData;
}

export class CatalogData extends Translate{
  id: number ;
  code: string ;
}

export class Catalog extends BaseModel{
  data: Array<CatalogData> = [];
}
