import {BaseModel} from './base.model';
import {Translate} from "./translate.model";
import {ProdItem} from "./product.model";


export class SingleCart extends BaseModel{
  data: CartData;
}

export class CartData{
  id: number;
  checked: number;
  sessions_id: number;
  created_at: string;
  updated_at: string;
  statistics: Array<Statistics>;
  product_items: Array<ProductItems> = [];
  totals: Totals  = new Totals();
}

export class Cart extends BaseModel{
  data: CartData = new CartData();
}

export class Statistics{
  id: number;
  product_items_id: number;
  carts_id: number;
  pages_id: number;
  quantity: number;
  is_added: number;
  created_at: string;
  updated_at: string;
  page: {
    id: number;
    name: string;
    code: string;
  }
}

export class ProductItems extends ProdItem{
  product_info: ProductInfo;
}

export class ProductInfo extends Translate{
    image: string;
    sku: string;
}

export class Totals{
  coupon: {
    code: string,
    value: string
  };
  total: number ;
  sub_total: number;

  constructor(){
    this.coupon = {
      code: '',
      value: ''
    };
  }
}
