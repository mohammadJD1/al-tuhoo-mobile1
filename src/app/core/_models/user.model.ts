import {BaseModel} from './base.model';
import {Translate} from "./translate.model";
import {ProductData} from "./product.model";


export class SingleUser extends BaseModel{
  data: UserData;
}

export class UserData extends Translate{
  id: number ;
  username: string ;
  first_name: string ;
  last_name: string ;
  email: string;
  mobile: string;
  birthday: string ;
  home_address1: null ;
  home_address2: null ;
  work_address: null ;
  location_address1: string ;
  location_address2: string ;
  photo: string ;
  remember_token: string ;
  created_at: string;
  updated_at: string;
}

export class User extends BaseModel{
  data: Array<UserData> = [];
}

