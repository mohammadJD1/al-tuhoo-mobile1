import {BaseModel} from './base.model';
import {ArticlesPageData} from "./articles-page.model";

export class Translate{
  translations: {
    en: {
      title: string,
      description: string,
      text: string,
      tags: string
    },
    ar: {
      title: string,
      description: string,
      text: string,
      tags: string
    }
  };
  constructor(){
    this.translations = {
      en: {
        title: '',
        description: '',
        text: '',
        tags: ''
      },
      ar: {
        title: '',
        description: '',
        text: '',
        tags: ''
      }
    };
  }
}

export class SingleArticle extends BaseModel{
  data: ArticleData = new ArticleData();
}

export class ArticleData extends Translate{
  id: number ;
  main_image: string ;
  date: string ;
  article_page: ArticlesPageData
}

export class Article extends BaseModel{
  data: Array<ArticleData> = [];
}
