import {BaseModel} from './base.model';
import {Translate} from "./translate.model";


export class SingleSliders extends BaseModel{
  data: SlidersData;
}

export class SlidersData extends Translate{
  id:  number ;
  image: string ;
  url: string ;
  enabled: number ;
  created_at: string ;
  updated_at: string;
}

export class Sliders extends BaseModel{
  data: Array<SlidersData> = [];
}
