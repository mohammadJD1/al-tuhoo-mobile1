import {BaseModel} from './base.model';

export class Translate{
  translations: {
    en: {
      name: string,
      symbol: string
    },
    ar: {
      name: string,
      symbol: string
    }
  };
  constructor(){
    this.translations = {
      en: {
        name: '',
        symbol: ''
      },
      ar: {
        name: '',
        symbol: ''
      }
    };
  }
}

export class SingleCurrency extends BaseModel{
  data: CurrencyData;
}

export class CurrencyData extends Translate{
  id: number ;
  code: string ;
  is_base_currency: number ;
  exchange_rate: number ;

}

export class Currency extends BaseModel{
  data: Array<CurrencyData> = [];
}

