import {BaseModel} from './base.model';
import {Translate} from "./translate.model";


export class SingleCategory extends BaseModel{
  data: CategoryData = new CategoryData();
}

export class CategoryData extends Translate{
  code: string ;
  id: number ;
  image: string ;
  parent_category_id: number ;
  childs: any[] = [];
}

export class Category extends BaseModel{
  data: Array<CategoryData> = [];
}
