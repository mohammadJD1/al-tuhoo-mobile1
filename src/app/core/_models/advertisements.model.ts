import {BaseModel} from './base.model';

export class Translate{
  translations: {
    en: {
      title: string ;
      sub_title: string ;
      price: string ;
      discount: string ;
    },
    ar: {
      title: string ;
      sub_title: string ;
      price: string ;
      discount: string ;
    }
  };
  constructor(){
    this.translations = {
      en: {
        title: '',
        sub_title: '',
        price: '',
        discount: '',
      },
      ar: {
        title: '',
        sub_title: '',
        price: '',
        discount: '',
      }
    };
  }
}

export class SingleAdvertisements extends BaseModel{
  data: AdvertisementsData;
}

export class AdvertisementsData extends Translate{
  active: number;
  id: number;
  image: string;
  position: string;
  url: string;
}

export class Advertisements extends BaseModel{
  data: Array<AdvertisementsData> = [];
}

