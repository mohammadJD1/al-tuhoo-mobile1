import {BaseModel} from './base.model';
import {Translate} from "./translate.model";


export class SingleOrderList extends BaseModel{
  data: OrderListData;
}

export class OrderListData extends Translate{
  coupons: string;
  created_at:string
  discount: number;
  notes: string;
  order_id: number;
  // order_rows: ;
  order_status: string;
  payment_fee: number;
  payment_method: string;
  shipment_fee: number;
  shipping_zone: string;
  sub_total: number;
  tax_total: number;
  total: number;
}

export class OrderList extends BaseModel{
  data: Array<OrderListData> = [];
}
