import {BaseModel} from './base.model';
import {Translate} from "./translate.model";
import {ProductData} from "./product.model";
import {UserData} from "./user.model";


export class SingleQuestion extends BaseModel{
  data: QuestionData;
}

export class QuestionData extends Translate{
  id: number ;
  question: string ;
  answer: string ;
  created_at: string ;
  user: UserData ;
  product: ProductData ;
  answered_by: UserData ;
}

export class Question extends BaseModel{
  data: Array<QuestionData> = [];
}

