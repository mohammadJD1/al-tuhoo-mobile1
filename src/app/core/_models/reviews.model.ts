import {BaseModel} from './base.model';
import {Translate} from "./translate.model";


export class SingleReviews extends BaseModel{
  data: ReviewsData;
}

export class ReviewsData extends Translate{
  id: number ;
  review: string ;
  rating: number ;
  approved: number ;
  user: User = new User();
  users_id: number ;
  products_id: number ;
  created_at: string ;
  updated_at: string ;
}

export class Reviews extends BaseModel{
  data: Array<ReviewsData> = [];
}

export class User{
  first_name: string ;
  id: number ;
  last_name: string ;
  username: string ;
}
