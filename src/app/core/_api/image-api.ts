import {Injectable} from '@angular/core';
import {BaseApiUrl} from '../_config/base-url';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {TokenService} from "./token.service";

@Injectable({
  providedIn: 'root'
})
export class ImageApi {

  // url:string = BaseApiUrl + '/image';
  url:string = BaseApiUrl + '/upload';

  constructor(
    private http: HttpClient,
    public _tokenService: TokenService,
  ) {
  }

  get():Observable<any> {
    return this.http.get<any>(this.url, this._tokenService.getHttpOptions());
  }

  postFile(fileToUpload: any): Observable<any> {
    // const HttpUploadOptions = {
    //   headers: new HttpHeaders(
    //     {
    //       'Authorization': 'Bearer '+this._tokenService.get(),
    //     })
    // }
    // const httpOptions = {
    //   headers: new HttpHeaders(
    //       { 'Content-Type': 'multipart/form-data' },
    //       )
    // };

    const formData: FormData = new FormData();
    formData.append('image', fileToUpload, fileToUpload.name);
    return this.http.post(this.url, formData);

  }
}
