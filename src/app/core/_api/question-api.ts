import {Injectable} from '@angular/core';
import {BaseApiUrl} from '../_config/base-url';
import {BehaviorSubject, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {TokenService} from "./token.service";
import {Cart} from "../_models/cart.model";

@Injectable({
  providedIn: 'root'
})
export class QuestionApi {

  url:string = BaseApiUrl + '/questions';
  prodUrl:string = BaseApiUrl + '/products';
  userUrl:string = BaseApiUrl + '/users';
  questionName:string = '/questions';

  constructor(
    private http: HttpClient,
    public _tokenService: TokenService,
  ) {
  }

  askQuestionAboutProd(productId, question):Observable<any> {
    return this.http.post<any>(this.prodUrl+'/'+productId+this.questionName,
      {
        "question": question
      },
      this._tokenService.getHttpOptions());
  }
  getAllQuestionsOfProduct(productId):Observable<any> {
    return this.http.get<any>(this.prodUrl+'/'+productId+this.questionName,
      this._tokenService.getHttpOptions());
  }

  getAnsweredQuestionsOfProduct(productId):Observable<any> {
    return this.http.get<any>(this.prodUrl+'/'+productId+this.questionName+'/answered',
      this._tokenService.getHttpOptions());
  }

  getAskedQuestionsByUserID(userId):Observable<any> {
    return this.http.get<any>(this.userUrl+'/'+userId+this.questionName+'/asked',
      this._tokenService.getHttpOptions());
  }

}
