
import {Injectable} from '@angular/core';
import {BaseApiUrl} from '../_config/base-url';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {TokenService} from "./token.service";

@Injectable({
  providedIn: 'root'
})
export class CategoryApi {

  url:string = BaseApiUrl + '/categories';

  constructor(
    private http: HttpClient,
    public _tokenService: TokenService,
  ) {
  }

  get():Observable<any> {
    return this.http.get<any>(this.url, this._tokenService.getHttpOptionsWithoutAuth());
  }

  getCatTree():Observable<any> {
    return this.http.get<any>(this.url+'/tree', this._tokenService.getHttpOptionsWithoutAuth());
  }

  getCategoriesChildren(catId):Observable<any> {
    return this.http.get<any>(this.url+'/'+catId+'/children', this._tokenService.getHttpOptionsWithoutAuth());
  }

  getCategoriesChildrenByName(catName):Observable<any> {
    return this.http.get<any>(this.url+'/children/by_name?category_name='+catName, this._tokenService.getHttpOptionsWithoutAuth());
  }

  getCategoryById(catId):Observable<any> {
    return this.http.get<any>(this.url+'/'+catId+'/children', this._tokenService.getHttpOptionsWithoutAuth());
  }

  getCategoryByName(catName):Observable<any> {
    return this.http.get<any>(this.url+'/by_name?category_name='+catName, this._tokenService.getHttpOptionsWithoutAuth());
  }

  getCategoryParentsByName(catName):Observable<any> {
    return this.http.get<any>(this.url+'/parents/by_name?category_name='+catName, this._tokenService.getHttpOptionsWithoutAuth());
  }
}
