import {Injectable} from '@angular/core';
import {BaseApiUrl} from '../_config/base-url';
import {BehaviorSubject, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {TokenService} from "./token.service";
import {Cart} from "../_models/cart.model";

@Injectable({
  providedIn: 'root'
})
export class CartApi {

  url:string = BaseApiUrl + '/carts';

  private cartNotifSource = new BehaviorSubject(0);
  currentCartNotif = this.cartNotifSource.asObservable();

  private cartItemsSource = new BehaviorSubject(new Cart());
  currentCartItems = this.cartItemsSource.asObservable();

  constructor(
    private http: HttpClient,
    public _tokenService: TokenService,
  ) {
    this.changeCartNotif();
      this.getCart().subscribe(resp=>{
        this.changeCartItems(resp);
      });
  }

  getCart():Observable<Cart> {
    return this.http.get<Cart>(this.url, this._tokenService.getHttpOptions());
  }

  update(productId,quantity): Observable<any> {
    return this.http.post<any>(this.url + '/update',
      {
        "items": [{
          "product_id": productId,
          "quantity": quantity
        }]
      }
      , this._tokenService.getHttpOptions());
  }

  checkout(): Observable<any> {
    return this.http.post<any>(this.url + '/checkout',
      {}
      , this._tokenService.getHttpOptions());
  }

  addCoupon(cartId , code): Observable<any> {
    return this.http.post<any>(this.url + '/'+cartId+'/apply_coupon',
      {
        "code": code
      }
      , this._tokenService.getHttpOptions());
  }

  /**
   *
   * @param productId this is product item inside product
   * @param quantity
   */
  addProduct(productId,quantity ):Observable<any> {
    return this.http.post<any>(this.url+'/add',
      {
        "item": {
          "product_id": productId,
          "quantity": quantity
        }
      }
      , this._tokenService.getHttpOptions());
  }

  changeCartNotif() {
    this.getCart().subscribe(resp=>{
      this.cartNotifSource.next(resp.data.product_items.length);
    });
  }

  changeCartItems(items) {
    this.cartItemsSource.next(items);
  }
}
