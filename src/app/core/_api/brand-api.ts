import {Injectable} from '@angular/core';
import {BaseApiUrl} from '../_config/base-url';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {TokenService} from "./token.service";

@Injectable({
  providedIn: 'root'
})
export class BrandsApi {

  url:string = BaseApiUrl + '/brands';

  constructor(
    private http: HttpClient,
    public _tokenService: TokenService,
  ) {
  }

  get():Observable<any> {
    return this.http.get<any>(this.url, this._tokenService.getHttpOptions());
  }

}
