import {Injectable} from '@angular/core';
import {BaseApiUrl} from '../_config/base-url';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
// import {HttpOptions} from '../_config/http-options';
import {TokenService} from './token.service';

@Injectable({
  providedIn: 'root'
})
export class ProductApi {

  url:string = BaseApiUrl + '/products';
  categoriesUrl:string = BaseApiUrl + '/categories';
  reviewsUrl:string = '/reviews';
  relatedUrl:string = '/related';
  bestSellerUrl:string = this.url+'/best_seller';

  constructor(
    private http: HttpClient,
    public _tokenService: TokenService,
  ) {
  }

  get():Observable<any> {
    return this.http.get<any>(this.url, this._tokenService.getHttpOptions());
  }

  getById(id):Observable<any> {
    return this.http.get<any>(this.url+'/'+id, this._tokenService.getHttpOptions());
  }

  getByName(prodName):Observable<any> {
    return this.http.get<any>(this.url+'/by_name?product_name='+prodName, this._tokenService.getHttpOptions());
  }

  getByCatName(catName):Observable<any> {
    return this.http.get<any>(this.url+'/by_cat_name?cat_name='+catName, this._tokenService.getHttpOptions());
  }

  getByCategoryID(catId):Observable<any> {
    return this.http.get<any>(this.categoriesUrl+'/'+catId+'/products', this._tokenService.getHttpOptions());
  }

  /*reviews*/

  getReviewsById(prodId):Observable<any> {
    return this.http.get<any>(this.url+'/'+prodId+this.reviewsUrl, this._tokenService.getHttpOptions());
  }

  getReviewsByName(prodName):Observable<any> {
    return this.http.get<any>(this.url+'/by_name/reviews?product_name='+prodName, this._tokenService.getHttpOptions());
  }

  reviewProduct(prodId,review,rating ):Observable<any> {
    return this.http.post<any>(this.url+'/by_name/reviews?product_name='+prodId,
      {
        "review": {
          "review": review,
          "rating": rating
        }
      }
      ,this._tokenService.getHttpOptions());
  }

  /* Related product */

  getRelatedProd(prodId):Observable<any> {
    return this.http.get<any>(this.url+'/'+prodId+this.relatedUrl, this._tokenService.getHttpOptions());
  }

  getRelatedProdByName(prodName):Observable<any> {
    return this.http.get<any>(this.url+'/by_name/related?product_name='+prodName, this._tokenService.getHttpOptions());
  }
  /* best_seller product */

  getBestSellerProd():Observable<any> {
    return this.http.get<any>(this.bestSellerUrl, this._tokenService.getHttpOptions());
  }

  search(query):Observable<any> {
    return this.http.get<any>(this.url+"/search?q="+query, this._tokenService.getHttpOptions());
  }
}
