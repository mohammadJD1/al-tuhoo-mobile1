import {Injectable} from '@angular/core';
import {BaseApiUrl} from '../_config/base-url';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {TokenService} from "./token.service";
import {User} from "./user.service";

@Injectable({
  providedIn: 'root'
})
export class ContactUsApi {

  url:string = BaseApiUrl + '/contact_us';

  constructor(
    private http: HttpClient,
    public _tokenService: TokenService,
  ) {
  }

  submit(contact_us): Observable<User> {

    return this.http.post<User>(this.url,
      {
        contact_us
      }
      , this._tokenService.getHttpOptions()).pipe();
  }

}
