﻿import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {TokenService} from './token.service';
import {BaseApiUrl} from "../_config/base-url";
import {Router} from "@angular/router";
import {UserData} from "../_models/user.model";
// import {HttpOptions} from "../_config/http-options";


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public _login_url = BaseApiUrl + `/login`;
  public _logout_url = BaseApiUrl + `/auth/logout`;
  public _signup_url = BaseApiUrl + `/customer/signup`;
  public _forget_password_url = BaseApiUrl + `/password/email`;
  public _reset_password_url = BaseApiUrl + `/password/reset`;
  public _update_password_url = BaseApiUrl + `/password/update`;
  public usersUrl = BaseApiUrl + `/auth/user`;
  public compareUrl = BaseApiUrl + `/compare`;
  public wishlistUrl = BaseApiUrl + `/wishlist`;
  public orderListUrl = BaseApiUrl + `/order/list`;
  public _update_user = BaseApiUrl + `/customer/update`;

  private compareListNotifSource = new BehaviorSubject(0);
  currentCompareListNotif = this.compareListNotifSource.asObservable();

  private wishListNotifSource = new BehaviorSubject(0);
  currentWishListNotif = this.wishListNotifSource.asObservable();

  constructor(
    private http: HttpClient,
    private tokenService: TokenService,
    private router: Router,
  ) {
    if(this.get()){
      // this.changeCompareListNotif();
      // this.changeWishListNotif();
    }
  }


  loginAccessToken(email, password, remember_me) {
    const headers = new HttpHeaders().set('Accept', 'application/json');
    return this.http.post<any[]>(this._login_url, {
      headers: headers, params: JSON.stringify({
        'email': email,
        'password': password,
        'remember_me': remember_me
      })
    });
  }

  getUser(): Observable<User> {
    return this.http.get<User>(this.usersUrl, {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Authorization': this.tokenService.get()
      })
    }).pipe();
  }

  login(user): Observable<User> {
    return this.http.post<User>(this._login_url, user, httpOptions).pipe();
  }

  logout() {
    // return this.http.get(this._logout_url, this.tokenService.getHttpOptions()).pipe();
    this.remove();
    this.tokenService.remove();
    this.router.navigate(['/']);
  }

  signup(user): Observable<User> {
    let httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        }
        )
    };
    return this.http.post<User>(this._signup_url, user, httpOptions).pipe();
  }

  forgetPass(user): Observable<User> {
    return this.http.post<User>(this._forget_password_url, user, httpOptions).pipe();
  }

  resetPass(user): Observable<User> {
    return this.http.post<User>(this._reset_password_url, user, httpOptions).pipe();
  }

  updatePass(user): Observable<User> {
    return this.http.post<User>(this._update_password_url, user, this.tokenService.getHttpOptions()).pipe();
  }

  handle(user) {
    this.set(user);
  }

  set(user: User) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  get() {
    if (localStorage.getItem("user") == null || localStorage.getItem("user") == undefined || localStorage.getItem("user") == "undefined") {
      return null;
    } else {
      return JSON.parse(localStorage.getItem('user'));
    }
  }

  remove() {
    localStorage.removeItem('user');
  }

  updateUserStorage(user){//update user in user storage
    let item:any;
    item = this.get();
    item.user = user;
    this.set(item);
  }

  /*** Compare ***/

  getComparedProd():Observable<any>{
    return this.http.get<any>(this.compareUrl,this.tokenService.getHttpOptions());
  }

  addComparedProd(id):Observable<any>{
    return this.http.post<any>(this.compareUrl,{'product_id':id},this.tokenService.getHttpOptions());
  }

  deleteComparedProd(id):Observable<any>{
    return this.http.delete<any>(this.compareUrl+'/'+id,this.tokenService.getHttpOptions());
  }

  changeCompareListNotif() {
    this.getComparedProd().subscribe(resp=>{
      this.compareListNotifSource.next(resp.data.length);
    });
  }

  /*** End Compare ***/

  /*** WishList ***/

  getWishlistProd():Observable<any>{
    return this.http.get<any>(this.wishlistUrl,this.tokenService.getHttpOptions());
  }

  addWishlistProd(id):Observable<any>{
    return this.http.post<any>(this.wishlistUrl,{'product_id':id},this.tokenService.getHttpOptions());
  }

  deleteWishlistProd(id):Observable<any>{
    return this.http.delete<any>(this.wishlistUrl+'/'+id,this.tokenService.getHttpOptions());
  }
  changeWishListNotif() {
    this.getWishlistProd().subscribe(resp=>{
      this.wishListNotifSource.next(resp.data.length);
    });
  }

  /*** End WishList ***/


  /*** OrderList ***/

  getOrderList():Observable<any>{
    return this.http.get<any>(this.orderListUrl,this.tokenService.getHttpOptions());
  }

  /*** End OrderList ***/

  /*** Update User ***/

  updateUser(user:UserData):Observable<any>{
    return this.http.post<any>(this._update_user,
        {
          username: user.username,
          email: user.email,
          mobile: user.mobile,
          home_address1: user.home_address1,
          home_address2: user.home_address2,
          work_address: user.work_address,
          location_address1: user.location_address1,
          location_address2: user.location_address2,
        },this.tokenService.getHttpOptions());
  }

  /*** End Update User ***/
}

export interface User {
  id: number;
  name: string;
  username: string;
  email: string;
  mobile: string;
  home_address1: string;
  home_address2: string;
  work_address: string;
  location_address1: string;
  location_address2: string;
  password: string;
  password_confirmation: string;
  remember_me: boolean;
  created_at: Date;
  updated_at: Date;
}
