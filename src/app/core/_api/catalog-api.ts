import {Injectable} from '@angular/core';
import {BaseApiUrl} from '../_config/base-url';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {TokenService} from "./token.service";

@Injectable({
  providedIn: 'root'
})
export class CatalogApi {

  url:string = BaseApiUrl + '/catalogs';
  featuredProductUrl: string = this.url+'/2/products';

  constructor(
    private http: HttpClient,
    public _tokenService: TokenService,
  ) {
  }

  get():Observable<any> {
    return this.http.get<any>(this.url, this._tokenService.getHttpOptionsWithoutAuth());
  }

  getFeaturedProducts():Observable<any> {
    return this.http.get<any>(this.featuredProductUrl, this._tokenService.getHttpOptionsWithoutAuth());
  }

  getProductsByCatalogId(id):Observable<any> {
    return this.http.get<any>(this.url+'/'+id+'/products', this._tokenService.getHttpOptionsWithoutAuth());
  }
}
