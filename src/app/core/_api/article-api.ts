import {Injectable} from '@angular/core';
import {BaseApiUrl} from '../_config/base-url';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {TokenService} from "./token.service";

@Injectable({
  providedIn: 'root'
})
export class ArticleApi {

  url:string = BaseApiUrl + '/articles';
  articlePagesUrl:string = BaseApiUrl + '/article_pages';

  constructor(
    private http: HttpClient,
    public _tokenService: TokenService,
  ) {
  }

  get():Observable<any> {
    return this.http.get<any>(this.url, this._tokenService.getHttpOptions());
  }

  getArticleByName(articleName):Observable<any> {
    return this.http.get<any>(BaseApiUrl+'/by_name/articles?article_name='+articleName, this._tokenService.getHttpOptions());
  }

  getArticlesByPageName(pageName):Observable<any> {
    return this.http.get<any>(BaseApiUrl+'/by_page_name/articles?page_name='+pageName, this._tokenService.getHttpOptions());
  }

  getAllPages():Observable<any> {
    return this.http.get<any>(this.articlePagesUrl, this._tokenService.getHttpOptions());
  }

}
