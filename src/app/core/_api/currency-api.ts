import {Injectable} from '@angular/core';
import {BaseApiUrl} from '../_config/base-url';
import {BehaviorSubject, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {TokenService} from "./token.service";
import {CurrencyData} from "../_models/currency.model";

@Injectable({
  providedIn: 'root'
})
export class CurrencyApi {

  url:string = BaseApiUrl + '/currency';

  private currentCurrencySource = new BehaviorSubject(new CurrencyData());
  currentCurrency = this.currentCurrencySource.asObservable();

  constructor(
    private http: HttpClient,
    public _tokenService: TokenService,
  ) {
  }

  get():Observable<any> {
    return this.http.get<any>(this.url, this._tokenService.getHttpOptions());
  }

  changeCurrentCurrency(currency) {
      this.currentCurrencySource.next(currency);
  }

  getDefaultCurrency() {
    if (localStorage.getItem("currency") == null) {
      localStorage.setItem("currency", "kwd");
      // this.currentLang = this.currentLangObj['en'];
    }
    let currency = localStorage.getItem("currency");
    return currency;
  }

  setDefaultCurrency(currency) {
      localStorage.setItem("currency", currency);
  }
}
