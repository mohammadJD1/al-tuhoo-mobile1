import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/services/categories.service';
import { Router } from '@angular/router';
import { ShareUiService } from 'src/app/services/share_ui.service';
import {CategoryApi} from "../../core/_api/category-api";
import {TranslateService} from "@ngx-translate/core";
import {Category} from "../../core/_models/category.model";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
})
export class CategoriesComponent implements OnInit {
  categoriesData: any;
  loading:boolean = false;
  // constructor(private categorieService: CategoriesService,
  //             private router: Router,
  //             private shareUi: ShareUiService) { }
  //
  // ngOnInit() {
  //   this.categorieService.GetCategories().subscribe(data => {
  //     this.categoriesData = data.data;
  //     this.categoriesData.forEach(element => {
  //     });
  //   });
  // }

  items:Category = new Category();

  constructor(
      private _categoryApi: CategoryApi,
      public translate: TranslateService,
      private router: Router,
  ) {
  }

  ngOnInit() {
    this.initCategries();
  }

  initCategries() {
    this.loading = true;
    this._categoryApi.getCatTree().subscribe(resp => {
          this.items = resp;
          this.loading = false;
        },
        error => {
          this.loading = false;
        });
  }

  moveToSubCategorie(cat) {
    // this.shareUi.ShowErrorAlert(`This Categories don't have sub categories`, `No sub categories yet`);
    // console.log(`no`);
    this.router.navigate([`/members/products/${cat}`]);
  }

}
