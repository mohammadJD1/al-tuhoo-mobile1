import { Component, OnInit } from '@angular/core';
import {UserService} from "../../core/_api/user.service";
import {TokenService} from "../../core/_api/token.service";
import {TranslateService} from "@ngx-translate/core";
import {Product, ProductData} from "../../core/_models/product.model";
import {tuhooConfig} from "../../tuhoo.config";
import {CurrencyData} from "../../core/_models/currency.model";
import {CurrencyApi} from "../../core/_api/currency-api";
import {ShareUiService} from "../../services/share_ui.service";

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss'],
})
export class FavoritesComponent implements OnInit {

  BASEURL = `${tuhooConfig.mainUrl}`;
  wishList: Product = new Product();
  isFav: Array<boolean>;
  stars: Array<any> = new Array<any>(5);
  math = Math;
  currentCurrency: CurrencyData ;
  isLoading:boolean = false;

  constructor(
      public _userService: UserService,
      public _tokenService: TokenService,
      public translate: TranslateService,
      private _currencyApi:CurrencyApi,
      private shareUi: ShareUiService,
  ) { }

  ngOnInit() {
    this._currencyApi.currentCurrency.subscribe(resp => this.currentCurrency = resp);
    this.initWishList();
  }

  initWishList(){
    this.isLoading = true;
    this.shareUi.ShowLoader();
    this._userService.getWishlistProd().subscribe(resp=>{
          this.isLoading = false;
          this.wishList = resp;
          this.isFav = new Array<boolean>(this.wishList.data.length);
          // this.isDeleting.fill(false);
          for(let i = 0 ; i<this.wishList.data.length;i++){
            this.isFav[i] = this.wishList.data[i].is_favorite;
          }
          setTimeout(() => {
            this.shareUi.loading.dismiss();
          }, 1000);
        },
        error => {
          this.isLoading = false;
          setTimeout(() => {
            this.shareUi.loading.dismiss();
          }, 1000);
        })
  }

  deleteFav(id, index){
    // this.isFav[i] = true;
    this._userService.deleteWishlistProd(id).subscribe(resp => {
          // this.isFav[i] = false;
          // this.toastr.success('ItemDeletedSuccessfully');
          // this.initWishList();
          this.isFav[index] = false;
          // this._userService.changeWishListNotif();
        },
        error => {
          // this.isFav[i] = false;
          // handlingError(error, this.toastr);
        });
  }


  addWishlistProd(prod:ProductData, index){
    if (this.isFav[index]) this.deleteFav(prod.id, index);
    // this.isLoading = true;
    else {
      this._userService.addWishlistProd(prod.id).subscribe(resp => {
            // this._userService.changeWishListNotif();
            // this.toastr.success('AddedSuccessfullyToWishlist');
            // this.isLoading = false;
            this.isFav[index] = true;
          },
          error => {
            // handlingError(error, this.toastr);
            // this.isLoading = false;
          });
    }
  }
}
