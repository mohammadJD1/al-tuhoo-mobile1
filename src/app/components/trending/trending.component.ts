import { Component, OnInit } from '@angular/core';
import {CatalogApi} from "../../core/_api/catalog-api";
import {TranslateService} from "@ngx-translate/core";
import {Catalog, CatalogData} from "../../core/_models/catalog.model";
import {Product, ProductData} from "../../core/_models/product.model";
import {CurrencyApi} from "../../core/_api/currency-api";
import {UserService} from "../../core/_api/user.service";
import {tuhooConfig} from "../../tuhoo.config";
import {CurrencyData} from "../../core/_models/currency.model";

@Component({
  selector: 'app-trending',
  templateUrl: './trending.component.html',
  styleUrls: ['./trending.component.scss'],
})
export class TrendingComponent implements OnInit {

  // catData= [
  //   {id: 1, code: "BS", translations: {en: {name: "Best Seller", description: "Best Seller"}, ar: {}}},
  // {id: 2, code: "FP",translations: {en: {name: "Featured products", description: "Featured products chosen by admin"}, ar: {}}},
  // {id: 3, code: "FP",translations: {en: {name: "Test", description: "Featured products chosen by admin"}, ar: {}}},
  // {id: 4, code: "FP",translations: {en: {name: "My test", description: "Featured products chosen by admin"}, ar: {}}},
  // ]
  sliderOptions = {
    speed: 300,
    autoplay: false,
    slidesPerView: 1.5,
    spaceBetween:10
  };
  catalog: Catalog = new Catalog();
  currentCat: CatalogData = new CatalogData();
  currentSliderIndex:number = 0;
  products: Product = new Product();
  stars: Array<any> = new Array<any>(5);
  math = Math;
  BASEURL = `${tuhooConfig.mainUrl}`;
  currentCurrency: CurrencyData ;
  isFav: Array<boolean>;
  loading:boolean = false;

  constructor(
      private _catalogApi: CatalogApi,
      private translate: TranslateService,
      private _currencyApi:CurrencyApi,
      public _userService: UserService,
  ) { }

  ngOnInit() {
    this._currencyApi.currentCurrency.subscribe(resp => this.currentCurrency = resp);
    this.getCatalog();
  }

  getCatalog(){
    this._catalogApi.get().subscribe(resp=>{
      this.catalog = resp;
      this.currentCat = this.catalog.data[0];
      this.initProducts();
      console.log("index: 0");
      console.log("currentCat:");
      console.log(this.currentCat);
    })
  }

  getIndex(slider) {
    slider.getActiveIndex().then(index=>{
      this.currentSliderIndex = index;
      this.currentCat = this.catalog.data[index];
      this.initProducts();
      console.log("index: "+index);
    })
  }

  initProducts() {
    this.loading = true;
    this.products = new Product();
    this._catalogApi.getProductsByCatalogId(this.currentCat.id).subscribe(resp => {
          this.products = resp;
          this.isFav = new Array<boolean>(this.products.data.length);
          for(let i = 0 ; i<this.products.data.length;i++){
            this.isFav[i] = this.products.data[i].is_favorite;
          }
          this.loading = false;
        },
        error => {
          this.loading = false;
        });
  }

  addWishlistProd(prod: ProductData, index) {
    if (this.isFav[index]) this.deleteFav(prod.id, index);
    // this.isLoading = true;
    else {
      this._userService.addWishlistProd(prod.id).subscribe(resp => {
            // this._userService.changeWishListNotif();
            // this.toastr.success('AddedSuccessfullyToWishlist');
            // this.isLoading = false;
            this.isFav[index] = true;
          },
          error => {
            // handlingError(error, this.toastr);
            // this.isLoading = false;
          });
    }
  }

  deleteFav(id, index){
    // this.isFav[i] = true;
    this._userService.deleteWishlistProd(id).subscribe(resp => {
          // this.isFav[i] = false;
          // this.toastr.success('ItemDeletedSuccessfully');
          // this.initWishList();
          this.isFav[index] = false;
          // this._userService.changeWishListNotif();
        },
        error => {
          // this.isFav[i] = false;
          // handlingError(error, this.toastr);
        });
  }
}
