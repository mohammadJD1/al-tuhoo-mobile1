import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {UserService} from "../../core/_api/user.service";
import {User, UserData} from "../../core/_models/user.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ConfirmPasswordValidator} from "../../public/confirm-password.validator";
import {ShareUiService} from "../../services/share_ui.service";
import {TokenService} from "../../core/_api/token.service";
import {tuhooConfig} from "../../tuhoo.config";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  @ViewChild('divright', {static: false}) private divright: ElementRef;
  @ViewChild('divleft', {static: false}) private divleft: ElementRef;
  sliderOptions = {
    initialSlide: 1,
    speed: 300,
    autoplay: false
  };

  BASEURL = `${tuhooConfig.mainUrl}`;

  user:UserData = new UserData();
  currentSliderIndex:number = 1;
  public passwordForm: FormGroup;
  public userForm: FormGroup;

  public homeAddress1: string;
  public homeAddress2: string;
  public workAddress: string;
  public locationAddress1: string;
  public locationAddress2: string;

  constructor(
      private _userService: UserService,
      private fb: FormBuilder,
      private router: Router,
      private shareUi: ShareUiService,
      public tokenService: TokenService,
  ) { }

  ngOnInit() {
    this.user = this._userService.get().user;
    // console.log("user");
    // console.log(this.user);

    this.passwordForm = this.fb.group({
          password: [null, Validators.required],
          confirmPassword: [null, Validators.required],
        },
        {
          validator: ConfirmPasswordValidator.MatchPassword
        });

    this.userForm = this.fb.group({
          username: [null, Validators.required],
          email: [null, Validators.compose([Validators.required, Validators.email])],
          mobile: [null, null],
        });
    this.initUserForm();

  }

  initUserForm(){
    let control = this.userForm.controls;
    control['username'].setValue(this.user.username);
    control['email'].setValue(this.user.email);
    control['mobile'].setValue(this.user.mobile);
    this.homeAddress1 = this.user.home_address1;
    this.homeAddress2 = this.user.home_address2;
    this.workAddress = this.user.work_address;
    this.locationAddress1 = this.user.location_address1;
    this.locationAddress2 = this.user.location_address2;
  }

  hideElementDiv() {
    this.divright.nativeElement.remove();
    this.divleft.nativeElement.remove();
  }

  showElementDiv() {
    this.divright.nativeElement.show();
    this.divleft.nativeElement.show();
    }

  getIndex(slider) {
    slider.getActiveIndex().then(index=>{
      this.currentSliderIndex = index;
    })
  }

  onSubmit() {

    if(this.currentSliderIndex==0) // Change Password Form
      this.changePassword();
    else //edit profile
      this.editProfile();
  }

  editProfile(){
    let controls = this.userForm.controls;

    const user:any = {
      username: controls['username'].value,
      email: controls['email'].value,
      mobile: controls['mobile'].value,
      home_address1: this.homeAddress1,
      home_address2: this.homeAddress2,
      work_address: this.workAddress,
      location_address1: this.locationAddress1,
      location_address2: this.locationAddress2,
    };

    /** check userForm */
    if (this.userForm.invalid) {
      Object.keys(controls).forEach(controlName =>
          controls[controlName].markAsTouched()
      );
      /** Error Messages**/
      if(this.isControlHasError('username','required'))//username
        this.shareUi.ShowErrorAlert('Error!','User Name is Required');
      if(this.isControlHasError('email','required'))//email
        this.shareUi.ShowErrorAlert('Error!','Email is Required');
      if(this.isControlHasError('email','email'))//email is correct
        this.shareUi.ShowErrorAlert('Error!','Email Must be valid');
      /** End Error Messages**/

      return;
    }

    this._userService.updateUser(user).subscribe(resp=>{
        if(user.username != this.user.username){
          this._userService.logout();
        }
        else{
          // console.log("resp.data");
          // console.log(resp.data);
          this._userService.updateUserStorage(resp.data);
          this.user = this._userService.get().user;
          // this.user = resp.data;
          // console.log("user");
          // console.log(this.user);
        }
    })
  }

  changePassword(){
    const controls = this.passwordForm.controls;
    /** check passwordForm */
    if (this.passwordForm.invalid) {
      Object.keys(controls).forEach(controlName =>
          controls[controlName].markAsTouched()
      );
        this.shareUi.ShowErrorAlert('Both passwords must match','');
      return;
    }

    const user = {
      password: controls['password'].value,
      password_confirmation: controls['confirmPassword'].value,
    };

    // this.isLoading = true;
    this.shareUi.ShowLoader();
    if (navigator.onLine) {
      this._userService.updatePass(user).subscribe((data: any) => {
            this._userService.logout();
            setTimeout(() => {
              this.shareUi.loading.dismiss();
            }, 1000);
            // this.toastr.success('PasswordSuccessfullyUpdated');
            this.router.navigate(['/public/login']);
          }, (error: any) => {
            const errorData = error.error.errors;
            setTimeout(() => {
              this.shareUi.loading.dismiss();
              if (error.status === 422) {
                Object.keys(errorData.password).forEach((value) => {
                  this.shareUi.ShowErrorAlert('Something went wrong', errorData.password[value]);
                });
              }
              if (error.status === 401) {
                // this.toastr.error('Unauthorized');
                this.shareUi.ShowErrorAlert('Something went wrong', 'Unauthorized');
              }
              else{
                this.shareUi.ShowErrorAlert('Something went wrong', '');
              }
            }, 1000);
          },
          () => {
            // this.userService.set(this.userInformation);
          }
      );
    }else {
      this.shareUi.ShowErrorAlert('No internet conection', 'check if wifi is working');
    }
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.userForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}
