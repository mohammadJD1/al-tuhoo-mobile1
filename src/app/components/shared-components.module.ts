import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ProfileComponent } from './profile/profile.component';
import { TrendingComponent } from './trending/trending.component';
import { CategoriesComponent } from './categories/categories.component';
import {FavoritesComponent} from "./favorites/favorites.component";
import {RouterModule} from "@angular/router";
import {SpinnerModule} from "./spinner/spinner.module";

@NgModule({
  declarations: [
    ProfileComponent,
    TrendingComponent,
    CategoriesComponent,
    FavoritesComponent
  ],
    imports: [
        CommonModule,
        IonicModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule,
        SpinnerModule,
    ],
  exports: [ProfileComponent, TrendingComponent, CategoriesComponent, FavoritesComponent]

})
export class SharedComponentsModule {}
