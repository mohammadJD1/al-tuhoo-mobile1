import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProductDetailPage } from './product-detail.page';
import {StarRatingModule} from "angular-star-rating";

const routes: Routes = [
  {
    path: '',
    component: ProductDetailPage
  },
  {
    path: ':id',
    component: ProductDetailPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        StarRatingModule.forRoot(),
    ],
  declarations: [ProductDetailPage]
})
export class ProductDetailPageModule {}
