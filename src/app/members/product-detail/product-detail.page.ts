import {Component, OnInit} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {Attributes, ProdItem, Product, ProductData} from "../../core/_models/product.model";
import {FormControl} from "@angular/forms";
import {Question} from "../../core/_models/questions.model";
import {CurrencyData} from "../../core/_models/currency.model";
import {ProductApi} from "../../core/_api/product-api";
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../../core/_api/user.service";
import {CartApi} from "../../core/_api/cart-api";
import {QuestionApi} from "../../core/_api/question-api";
import {CurrencyApi} from "../../core/_api/currency-api";
import {tuhooConfig} from "../../tuhoo.config";
import {Reviews} from "../../core/_models/reviews.model";
import {ShareUiService} from "../../services/share_ui.service";
import {handlingError} from "../../core/_services/handling-error.service";
import {TokenService} from "../../core/_api/token.service";

@Component({
    selector: 'app-product-detail',
    templateUrl: './product-detail.page.html',
    styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {
    name = new FormControl(1);

    product: ProductData = new ProductData();
    relatedProd: Product = new Product();
    quantity: number = 0;// quantity for all sizes.
    currentSize: any;
    sizes: any = [];

    currentItemsAttr: Attributes[] = [];
    currentProdItem: ProdItem = new ProdItem();
    id: any;
    isLoading: boolean = false;
    isLoadingQuestion: boolean = false;
    question: string = '';
    answeredQuestions: Question = new Question();
    currentCurrency: CurrencyData;
    currentUrl: string = window.location.href;
    stars: Array<any> = new Array<any>(5);
    math = Math;
    BASEURL = `${tuhooConfig.mainUrl}`;

    spescifications = true;
    show :boolean[];
    limitTo: number[];
    reviews: Reviews = new Reviews();
    segment: any = 'reviews';
    isFav: boolean;

    review: string = '';
    rating: number = 3;
    characterNum: number = 0;

    constructor(
        private _productApi: ProductApi,
        public translate: TranslateService,
        private route: ActivatedRoute,
        public _userService: UserService,
        private _cartApi: CartApi,
        private _questionApi: QuestionApi,
        private _currencyApi: CurrencyApi,
        private shareUi: ShareUiService,
        public _tokenService: TokenService,
    ) {
        this.product = this.product.init();
    }

    ngOnInit() {
        this._currencyApi.currentCurrency.subscribe(resp => this.currentCurrency = resp);
        this.route.params.subscribe(params => {
            this.id = params.id.split('-').join(' ');
            window.scrollTo(0, 0);
            this.initialiseState();
            this.initReviews();
        });
    }

    initialiseState() {
        this.quantity = 0;// quantity for all sizes.
        // this.currentSize = this.sizes[0];
        this.currentItemsAttr = [];
        this.currentProdItem = new ProdItem();
        this.isLoading = false;

        this.initProd();
        this.getRelatedProd();
    }

    ngAfterViewInit(): void {

    }

    initProd() {
        this._productApi.getByName(this.id).subscribe(resp => {
            this.product = resp.data;
            this.isFav = this.product.is_favorite;

            this.getSizes();
            this.getQuantity(this.product);
            this.getCurrentItem();
            // this.getAnsweredQuestionsOfProduct();
        });
    }

    getQuantity(prod: ProductData) {
        for (let i = 0; i < prod.items.length; i++) {
            this.quantity = this.quantity + prod.items[i].quantity;
        }
    }

    getCurrentItem(){
        let items:any = this.product.items;
        for(let i = 0 ; i<items.length;i++){
            let sizeTranslate = items[i].size==null?null:items[i].size.translations[this.translate.currentLang].name;
            let color = items[i].color==null?null:items[i].color.translations[this.translate.currentLang].name;
            if(color!=null){
                sizeTranslate = sizeTranslate+' / '+color;
            }
            if(this.currentSize == sizeTranslate){ // get item according to selected size
                this.currentProdItem = items[i];
                this.currentItemsAttr = items[i].items_attributes;
                if(this.currentProdItem.quantity == 0) this.name = new FormControl(0);
                return;
            }
        }
        this.currentItemsAttr = [];
        this.currentProdItem = new ProdItem();
        this.name = new FormControl(0);
    }

    getSizes(){
        let items:any = this.product.items;
        this.sizes = [];
        this.currentSize = items[0].size.translations[this.translate.currentLang].name;
        let color = items[0].color.translations[this.translate.currentLang].name;
        if(color!=null){
            this.currentSize = this.currentSize+' / '+color;
        }

        for(let i = 0 ; i<items.length;i++){
            //init sizes
            let sizeTranslate = items[i].size==null?null:items[i].size.translations[this.translate.currentLang].name;
            let color = items[i].color==null?null:items[i].color.translations[this.translate.currentLang].name;
            if(color!=null){
                sizeTranslate = sizeTranslate+' / '+color;
            }

            this.sizes.push(sizeTranslate);
            //End init sizes
        }
    }

    // addComparedProd(){
    //   this.isLoading = true;
    //   this._userService.addComparedProd(this.product.id).subscribe(resp=>{
    //         this._userService.changeCompareListNotif();
    //         this.toastr.success('AddedSuccessfullyToComparedList');
    //         this.isLoading = false;
    //       },
    //       error => {
    //         handlingError(error, this.toastr);
    //         this.isLoading = false;
    //       });
    // }
    //

    deleteFav(id){
        // this.isFav[i] = true;
        this._userService.deleteWishlistProd(id).subscribe(resp => {
                // this.initWishList();
                this.isFav = false;
                // this._userService.changeWishListNotif();
            },
            error => {
                // this.isFav[i] = false;
                // handlingError(error, this.toastr);
            });
    }

    addWishlistProd(prod:ProductData){
        if (this.isFav) this.deleteFav(prod.id);
        // this.isLoading = true;
        else {
            this._userService.addWishlistProd(prod.id).subscribe(resp => {
                    // this._userService.changeWishListNotif();
                    // this.toastr.success('AddedSuccessfullyToWishlist');
                    // this.isLoading = false;
                    this.isFav = true;
                },
                error => {
                    // handlingError(error, this.toastr);
                    // this.isLoading = false;
                });
        }
    }

    addProdToCart(quantity) {
      if (quantity == 0) {
          this.shareUi.ShowErrorAlert('Error!','Quantity must be at least 1');
          return;
      }
      this.shareUi.ShowLoader();
      this._cartApi.addProduct(this.currentProdItem.id, quantity).subscribe(resp => {
              setTimeout(() => {
                  this.shareUi.loading.dismiss();
              }, 1000);
            // this._cartApi.changeCartNotif();
            // this._cartApi.getCart().subscribe(resp=>{
            //   this._cartApi.changeCartItems(resp);
            // });
            // this.toastr.success('AddedSuccessfullyToCart');
            // this.isLoading = false;
          },
          error => {
              setTimeout(() => {
                  this.shareUi.loading.dismiss();
                  // this.shareUi.ShowErrorAlert('Something went wrong', '');
                  handlingError(error, this.shareUi);
              }, 1000);

            // this.isLoading = false;
          });
    }

    // askQuestion() {
    //   this.isLoadingQuestion = true;
    //   this._questionApi.askQuestionAboutProd(this.product.id, this.question).subscribe(resp => {
    //         this.toastr.success('PleaseWaitTheAdminToReply');
    //         this.isLoadingQuestion = false;
    //       },
    //       error => {
    //         handlingError(error, this.toastr);
    //         this.isLoadingQuestion = false;
    //       });
    // }

    getAnsweredQuestionsOfProduct() {
        this._questionApi.getAnsweredQuestionsOfProduct(this.product.id).subscribe(resp => {
                this.answeredQuestions = resp;
            },
            error => {
                // handlingError(error, this.toastr);
            });
    }

    getRelatedProd() {
        this._productApi.getRelatedProdByName(this.id).subscribe(resp => {
                this.relatedProd = resp;
            },
            error => {
                // handlingError(error, this.toastr);
            });
    }

    up(val) {
        if (val < this.currentProdItem.quantity) this.name.setValue(this.name.value + 1);
    }

    down(val) {
        if (this.name.value > 0) {
            this.name.setValue(this.name.value - 1);
        } else {
            this.name.setValue(0);
        }
    }

    segmentChanged(element) {
        // switch (element.detail.value) {
        //   case 'reviews':
        //     this.reviews = true;
        //     this.spescifications = false;
        //     break;
        //   case 'spescifications':
        //     this.reviews = false;
        //     this.spescifications = true;
        //     break;
        //   default:
        //     break;
        // }
    }

    // Reviews

    initReviews() {
        this._productApi.getReviewsByName(this.id).subscribe(resp => {
            this.reviews = resp;
            this.show = new Array(this.reviews.data.length);
            this.show.fill(false);
            this.limitTo = new Array(this.reviews.data.length);
            this.limitTo.fill(50);
        })
    }

    submitReview(){

        if(this._tokenService.get() == null){
            // this.toastr.error('PleaseLoginFirstToAddReview');
            this.shareUi.ShowErrorAlert('Error!','PleaseLoginFirstToAddReview');
            return;
        }

        if(this.review == ''){
            // this.toastr.error('PleaseAddComment');
            this.shareUi.ShowErrorAlert('Error!','PleaseAddComment');
            return;
        }
        // if(this.review.length < 50 ||this.review.length > 2000 ){
        //     // this.toastr.error('PleaseAddValidNumberOfCharacters');
        //     this.shareUi.ShowErrorAlert('Error!','PleaseAddValidNumberOfCharacters');
        //     return;
        // }

        // this.isLoading = true;
        this.shareUi.ShowLoader();
        this._productApi.reviewProduct(this.id,this.review, this.rating).subscribe(resp=>{
                // this.toastr.success('AddedSuccessfullyWaitingForAdminToApproveIt');
                this.initReviews();
                setTimeout(() => {
                    this.shareUi.loading.dismiss();
                }, 1000);
                this.review = '';
                // this.isLoading = false;
            },
            error => {
                setTimeout(() => {
                    this.shareUi.loading.dismiss();
                }, 1000);
                handlingError(error, this.shareUi);
                // this.isLoading = false;
            });
    }

    changeCharacterRating(){
        this.characterNum = this.review.length+1;
    }

    onRate(event){
        this.rating = event.rating;
    }

    showMore(index,sentence:string) {
        this.limitTo[index] = this.show[index] ? 50 : sentence.length;
        this.show[index] = !this.show[index];
    }
}
