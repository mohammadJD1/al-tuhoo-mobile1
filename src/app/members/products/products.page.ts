import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Category, CategoryData} from "../../core/_models/category.model";
import {Product, ProductData} from "../../core/_models/product.model";
import {ProductApi} from "../../core/_api/product-api";
import {CategoryApi} from "../../core/_api/category-api";
import {TranslateService} from "@ngx-translate/core";
import {tuhooConfig} from "../../tuhoo.config";
import {CurrencyData} from "../../core/_models/currency.model";
import {CurrencyApi} from "../../core/_api/currency-api";
import {UserService} from "../../core/_api/user.service";

@Component({
    selector: 'app-products',
    templateUrl: './products.page.html',
    styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {

    catID: any;
    featuredProducts: Product;
    categoriesChild: Category;
    currentCatParents: Array<CategoryData> = [];
    featuredLoading: boolean = false;
    stars: Array<any> = new Array<any>(5);
    math = Math;
    BASEURL = `${tuhooConfig.mainUrl}`;
    currentCurrency: CurrencyData ;
    isFav: Array<boolean>;

    constructor(
        private route: ActivatedRoute,
        private _productApi: ProductApi,
        private _categoryApi: CategoryApi,
        private translate: TranslateService,
        private _currencyApi:CurrencyApi,
        public _userService: UserService,
    ) {
    }

    ngOnInit() {
        this._currencyApi.currentCurrency.subscribe(resp => this.currentCurrency = resp);
        this.route.params.subscribe(params => {
            this.catID = params.catId.split('-').join(' ');
            this.initFeaturedProducts();
            this.getCategoryParntsByName();
        });
    }

    initFeaturedProducts() {
        this.categoriesChild = new Category();
        this.featuredLoading = true;
        this._categoryApi.getCategoriesChildrenByName(this.catID).subscribe(resp => {
            this.categoriesChild = resp;
        });

        this.featuredProducts = new Product();
        if (this.categoriesChild.data.length == 0) {
            this._productApi.getByCatName(this.catID).subscribe(resp => {
                    this.featuredProducts = resp;
                    this.featuredLoading = false;
                    this.isFav = new Array<boolean>(this.featuredProducts.data.length);
                    for(let i = 0 ; i<this.featuredProducts.data.length;i++){
                        this.isFav[i] = this.featuredProducts.data[i].is_favorite;
                    }
                },
                error => {
                    this.featuredLoading = false;
                });
        } else {
            this.featuredLoading = false;
        }
    }

    getCategoryParntsByName() {
        this._categoryApi.getCategoryParentsByName(this.catID).subscribe(resp => {
            this.currentCatParents = resp;
            // this.currentCat = this.currentCatParents[this.currentCatParents.length-1];
        });
    }

    addWishlistProd(prod: ProductData, index) {
        if (this.isFav[index]) this.deleteFav(prod.id, index);
        // this.isLoading = true;
        else {
            this._userService.addWishlistProd(prod.id).subscribe(resp => {
                    // this._userService.changeWishListNotif();
                    // this.toastr.success('AddedSuccessfullyToWishlist');
                    // this.isLoading = false;
                    this.isFav[index] = true;
                },
                error => {
                    // handlingError(error, this.toastr);
                    // this.isLoading = false;
                });
        }
    }

    deleteFav(id, index){
        // this.isFav[i] = true;
        this._userService.deleteWishlistProd(id).subscribe(resp => {
                // this.isFav[i] = false;
                // this.toastr.success('ItemDeletedSuccessfully');
                // this.initWishList();
                this.isFav[index] = false;
                // this._userService.changeWishListNotif();
            },
            error => {
                // this.isFav[i] = false;
                // handlingError(error, this.toastr);
            });
    }

}
