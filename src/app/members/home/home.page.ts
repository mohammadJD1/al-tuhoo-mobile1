import {MenuController, Platform} from '@ionic/angular';
import { ShareUiService } from 'src/app/services/share_ui.service';
import { CategoriesService } from 'src/app/services/categories.service';
import {Component, OnInit, Renderer2} from '@angular/core';
import { Router } from '@angular/router';
import {TranslateService} from "@ngx-translate/core";
import {MenuService} from "../../core/_services/menu.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  title = 'Home';
  categories = true;
  menuOpen:boolean;
  trending = false;
  profile = false;
  fav = false;
  categoriesData: any;
  parentcategory: any;


  sliderOptions = {
    speed: 300,
    autoplay: false
  };
  catName: any;

  constructor(
      private categorieService: CategoriesService,
      private router: Router,
      private shareUi: ShareUiService,
      private platform: Platform,
      public translate: TranslateService,
      private menu: MenuController,
      private _menuService: MenuService,
  ) {
  }

  ngOnInit() {
    this._menuService.currentMenuOpen.subscribe(resp => this.menuOpen = resp);
    // this.shareUi.ShowLoader();
    this.categorieService.GetCategories().subscribe(data => {
          this.categoriesData = data.data;
          // setTimeout(() => {
          //   this.shareUi.loading.dismiss();
          // }, 1000);
          // console.log(this.categoriesData);
          this.categoriesData.forEach(element => {
            // console.log(element.id);
            this.categorieService.GetCategoriesById(element.id).subscribe(iddata => {
              // console.log(iddata);
            });
          });
        },
        error => {
          // setTimeout(() => {
          //   this.shareUi.loading.dismiss();
          // }, 1000);
        });
  }

  segmentChanged(element) {
    switch (element.detail.value) {
      case 'categories':
        this.title = 'Home';
        this.categories = true;
        this.trending = false;
        this.profile = false;
        this.fav = false;
        break;
      case 'trending':
        this.title = 'Trending';
        this.categories = false;
        this.trending = true;
        this.profile = false;
        this.fav = false;
        break;
      case 'profile':
        this.title = 'Profile';
        this.categories = false;
        this.trending = false;
        this.profile = true;
        this.fav = false;
        break;
      case 'fav':
        this.title = 'Favorites';
        this.categories = false;
        this.trending = false;
        this.profile = false;
        this.fav = true;
        break;
      default:
        break;
    }
  }

  closeMenu() {
    this.menu.toggle();
    this._menuService.changeMenuOpen(!this.menuOpen);
  }

  moveToSubCategorie(cat) {
    this.shareUi.ShowErrorAlert(`This Categories don't have sub categories`, `No sub categories yet`);
    // console.log(`no`);
    this.router.navigate([`/members/sub-categorie/${cat.id}`]);

  }

}
