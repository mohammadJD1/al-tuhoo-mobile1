import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../../core/_api/user.service";

@Component({
  selector: 'app-chose-login',
  templateUrl: './chose-login.page.html',
  styleUrls: ['./chose-login.page.scss'],
})
export class ChoseLoginPage {

  constructor(
      private router: Router,
      private _userService: UserService
  ) {
    if(this._userService.get()!=null){
      this.router.navigate([`/members/home`]);
    }
  }
}
