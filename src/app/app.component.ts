import {Component, Renderer2, ViewEncapsulation} from '@angular/core';

import {MenuController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {AuthenticationService} from './services/authentication.service';
import {Router} from '@angular/router';
import {TranslateConfigService} from './services/translate-config.service';
import {TranslateService} from "@ngx-translate/core";
import {CurrencyApi} from "./core/_api/currency-api";
import {Currency, CurrencyData} from "./core/_models/currency.model";
import {MenuService} from "./core/_services/menu.service";
import {UserService} from "./core/_api/user.service";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  // encapsulation: ViewEncapsulation.None
})
export class AppComponent {

  selectedLanguage = 'en';
  currentLangObj: any = {
    en: {
      name: 'English'
    },
    ar: {
      name: 'العربية'
    }
  };
  currencyObj: Currency = new Currency();

  currentLang:any;
  currentCurrency:CurrencyData = new CurrencyData();
  selectedCurr:any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authenticationService: AuthenticationService,
    private router: Router,
    private translateConfigService: TranslateConfigService,
    private menu: MenuController,
    private renderer: Renderer2,
    public translate: TranslateService,
    private _currencyApi:CurrencyApi,
    private _menuService:MenuService,
    public _userService: UserService,
  ) {
    this.initializeApp();
    this.initLang();
    this.initCurrency();
    this.platform.backButton.subscribe(async () => {
      if (this.router.isActive('/members/home', true) && this.router.url === '/members/home') {
        navigator['app'].exitApp();
      }
    });
    // document.querySelector('#my-select').shadowRoot.querySelector('.select-icon-inner').setAttribute('style', 'opacity:1');

    // var style = document.createElement( 'style' );
    // style.innerHTML = '.select-icon { background-color:blue; }';
    // document.querySelector('#my-select').shadowRoot.appendChild( style );
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // this.authenticationService.authenticationState.subscribe(state => {
      //   if (state) {
      //     this.router.navigate(['members', 'home']);
      //   } else {
      //     this.router.navigate(['chose-login']);
      //   }
      // });

      // this.selectedLanguage = this.translateConfigService.getDefaultLanguage();

    });

  }


  closeMenu() {
    this.menu.toggle();
    this._menuService.changeMenuOpen(false);
  }
  languageChanged() {
    this.translateConfigService.setLanguage(this.selectedLanguage);
  }

  initLang(){
    if(localStorage.getItem("lang")==null){
      localStorage.setItem("lang", "en");
      this.renderer.setAttribute(document.querySelector('html'), 'lang', 'en');
      // this language will be used as a fallback when a translation isn't found in the current language
      this.translate.setDefaultLang('en');

      // the lang to use, if the lang isn't available, it will use the current loader to get them
      this.translate.use('en');
      this.currentLang = this.currentLangObj['en'];
    }
    else{
      let lang = localStorage.getItem("lang");
      // this language will be used as a fallback when a translation isn't found in the current language
      this.translate.setDefaultLang(lang);

      // the lang to use, if the lang isn't available, it will use the current loader to get them
      this.translate.use(lang);
      this.currentLang = this.currentLangObj[lang];
      this.renderer.setAttribute(document.querySelector('html'), 'lang', lang);
    }
  }

  initCurrency(){
    this._currencyApi.get().subscribe(resp=>{
      this.currencyObj = resp;
      for(let i = 0 ; i< this.currencyObj.data.length;i++){
        if(this.currencyObj.data[i].code == this._currencyApi.getDefaultCurrency()){
          this.currentCurrency = this.currencyObj.data[i];
          this.selectedCurr = this.currentCurrency.code;
          this._currencyApi.changeCurrentCurrency(this.currentCurrency);
        }
      }
    });
  }

  changeCurrency(){
    for(let i = 0 ; i< this.currencyObj.data.length;i++){
      if(this.currencyObj.data[i].code == this.selectedCurr){
        this.currentCurrency = this.currencyObj.data[i];
      }
    }
    this._currencyApi.changeCurrentCurrency(this.currentCurrency);
    this._currencyApi.setDefaultCurrency(this.currentCurrency.code);
  }

}
