import { Address } from './address.model';

export class Profile {

// tslint:disable-next-line:variable-name
access_token = '';
user = {
id: Number,
username: '',
first_name: '',
last_name: '',
email: '',
mobile: '',
birthday: '',
home_address1: null,
home_address2: null,
work_address: null,
location_address1: null,
location_address2: null,
photo: '',
created_at: '',
updated_at: ''
};
}
