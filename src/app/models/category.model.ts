import { Product } from './product.model';
import { Image } from './image.model';

export class Category {
		public id: number;
		public name: string;
		public images: Image[];
		public products: Product[];
		public updated_at: number;
}
