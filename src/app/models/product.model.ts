import { Image } from './image.model';

export class Product {
		public id: number;
		public additional: {
			name_en: string,
			name_ro: string,
			description_en: string,
			description_ro: string,
		};
		public name: string;
		public images: Image[];
		public price: number;
		public is_favorite: boolean;
		public attributes: any;
		public quantity = 0 ;
}
